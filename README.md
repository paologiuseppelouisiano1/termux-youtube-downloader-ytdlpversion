# Termux-YTD
#### Termux-YouTube-Downloader-YTDLPVersion
This is The latest Script For Downloading Youtube Videos with your Termux. Download any Youtube Video in 2 Clicks.


### Installation & Usage:
Install Termux-YouTube-Downloader-YTDLPVersion:
Step 1:

To download YouTube videos we need to Download the Termux-YTD project from GitHub, and also we have to install git to clone it from the GitHub, and will also require updating some stuff, but I don't want to waste your time, so I Combined all the commands in Single Big Command. Just copy and paste the below command in termux.


apt update -y && apt upgrade -y && pkg install git zlib ffmpeg -y && git clone https://gitlab.com/paologiuseppelouisiano1/termux-youtube-downloader-ytdlpversion && cd termux-youtube-downloader-ytdlpversion && bash install.sh && cd ~

please Allow the storage permission if you didn't already have.

Now Everything is perfectly set up just use the below method to download video from Youtube.

How To Download Videos with Termux.
Now everything is set up properly, you just have done 2 simple tiny steps to download any video from Youtube.



Step1:
Open youtube and select any Video you wanna download, Now you just Have to press on the Share button of the video you wanna download (You can also share Video Without opening then). 

Now Your Phone will give you a list of apps where you can share the Video. Just Select Termux.

Step2 :

Now you will see the list of formats and select one which is suitable for you and press enter and the downloading will start. For example : if you wanna download an mp3 version of the video then you just have to type 1 and press Enter to download it.



You will find all videos in your Youtube folder in your Internal storage.


### Features:
- See The Downloading Process.
- Download any Video in Just 2 Click.
- Select the Quality Of your Video from 240p to 4k Resolution.
- Downlaod Mp3, FLAC, M4A Version of the Video.
- Download Playlist to Best Quality Videos.



<br>
<br>


