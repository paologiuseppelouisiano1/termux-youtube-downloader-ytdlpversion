#!/data/data/com.termux/files/usr/bin/bash

# Project name : Termux-Youtube-Downloader-YTDLPVersion
# Date : 31-03-2022

#Probably make a varible with the file name 
#make it more easy to update!
echo "Removing Previous termux-url-opener entries in case of upgrade."
rm -rf /data/data/com.termux/files/home/bin/termux-url-opener
echo -e "\e[035m"  "Updating default packages\n"
apt update && apt upgrade -y

echo -e "\e[033m" "Installing python and other mandatory packages\n"
apt install zlib ffmpeg python termux-api -y

echo -e "\e[032m" "Requesting acces to storage\n"
sleep 2
echo -e "\e[032m" "Allow Storage Permission!"
sleep 2
termux-setup-storage 
sleep 5

echo -e "\e[034m"  "Installing yt-dlp\n"
pip install wheel
pip install yt-dlp
pip install youtube-dl
pip install youtube-comment-downloader
pip install phantomjs
pip install --upgrade pip wheel yt-dlp youtube-dl youtube-comment-downloader phantomjs

echo -e "\e[032m"  "Making the Youtube Directory to download the Vidoes\n"
mkdir ~/storage/shared/Youtube

echo -e "\e[036m"  "Creating yt-dlp folder for config\n"
mkdir -p ~/.config/yt-dlp

echo -e "Creating bin folder\n"
rm -rf ~/bin
mkdir ~/bin

echo -e "Creating Termux-URL-Opener Script.\n"
chmod u+x termux-url-opener
\cp -r termux-url-opener ~/bin/
#Oh hey Don't forget to chmod that file there!

chmod +x ~/bin/termux-url-opener
git reset --hard HEAD
cd ~
cd ~
cd ~
cd ~
cd ~
cd ~
cd ~
cd ~
echo -e "\n"
echo -e "\e[032m" "Process Complete!"
echo -e "\e[032m" "Now you can share any Youtube video with Termux and you will be ask to select the quality of your downloaded video and after that,It will be automatically Downloaded"
cd ~
cd ~
cd ~
cd ~
cd ~
cd ~
cd ~
cd ~
